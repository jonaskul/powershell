# Script to update FreeDNS and log the output.

# Change Path to desired log and Uri to your Direct URL
$LogPath = "C:\Scripts\Update-FreeDNS.log"
$Uri = "your_dns_url" # Enter your update URL


# No need to change this
Add-Content -Path $LogPath -Value "$(Get-Date) $(Invoke-WebRequest -Uri $Uri)"
