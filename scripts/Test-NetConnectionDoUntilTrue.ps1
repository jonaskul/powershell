#Test-NetConnection do until True

$Counter = 0

$RemoteAddress = Read-Host -Prompt "Remote DNS or IP"
$RemotePort = Read-Host -Prompt "Remote port"

do {
    Write-Host "Checking DNS"
    $TcpTest = Test-NetConnection $RemoteAddress -Port $RemotePort
} until ($TcpTest.PingSucceeded = $True)

Write-Host "DNS OK"

do {
    $Counter += 1
    Write-Host "Service test number: $Counter"
    $TcpTest = Test-NetConnection $RemoteAddress -Port $RemotePort
} until ($TcpTest.TcpTestSucceeded = $True)

$TcpTest
Write-Host "Counter          : $Counter"